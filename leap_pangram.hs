-- Here to satisfy GHC requirements (if were to not load as module, but create executable)
main :: IO ()
main = return ()

-- Input, a year. Output, True/False
leap :: Int -> Bool

leap n
    -- If the year is divisible by 4, then it has potential to be a leap year AND ...
    -- ... the year is NOT divisible by 100 or the year is divisible by 400, then it is a leap year
    | ((n `mod` 4 == 0) && not ((n `mod` 100 == 0) && not (n `mod` 400 == 0))) = True
    
    -- Everything else is not a leap year
    | otherwise = False

-- An alternative way to the above (more compact)
-- leap n = ((n `mod` 4 == 0) && not ((n `mod` 100 == 0) && not (n `mod` 400 == 0)))


pangram :: String -> Bool
    
pangram s
    -- Find every letter NOT in our input string
    -- Zip together uppercase and lowercase letters
    -- Only select letters that are either uppercase or lowercase that does not exist in our string
    -- If none can be found, then all letters of the alphabet exist in our string
    | [x | x <- zip ['a'..'z'] ['A'..'Z'], not (fst x `elem` s) && not (snd x `elem` s)] == [] = True -- Each iteration checks both upper and lower case -> (lower, upper) pairs
    | otherwise = False


-- An alternative way to the above (more compact)
-- pangram s = [x | x <- zip ['a'..'z'] ['A'..'Z'], not (fst x `elem` s) && not (snd x `elem` s)] == []