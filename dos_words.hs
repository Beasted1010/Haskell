
main :: IO ()
main = return ()





dos :: Integer -> Integer

dos n
    | n <= 0 = 0

dos n =
    let
        square_sum_first_n = (sum [1..n]) ^ 2
        sum_squares_first_n = sum [x * x | x <- [1..n]]
    in
        square_sum_first_n - sum_squares_first_n


-- Eat white spaces
eatSpaces :: String -> String

eatSpaces "" = ""
eatSpaces (c:s)
    | c == ' ' = eatSpaces s
    | otherwise = (c:s)
    
    
    
-- Extract a word
extractWord :: String -> String

extractWord "" = ""
extractWord (c:s)
    | c == ' ' = "" -- Want to check if next character is also a space or not
    | otherwise = c:(extractWord s)

    
    
-- Give the remaining string without any leading white spaces
eliminateWord :: String -> String

eliminateWord "" = ""
eliminateWord (c:s)
    | c == ' ' = eatSpaces s -- Return the remaining string without any white spaces
    | otherwise = eliminateWord s -- Lop off the character

    
    
s2w :: String -> [String]

s2w [] = []
s2w (c:s)
    | c == ' ' = s2w s
    | otherwise = (extractWord (c:s)):(s2w (eliminateWord (c:s)))





