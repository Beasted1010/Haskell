
main :: IO ()
main = return ()

binInt :: String -> Double

binInt "" = 0
binInt (x:xs)
    | x == ' ' && binInt xs == 0 = 0
    | x == '0' = binInt xs
    | otherwise = ( 2 ^ (length xs) ) + binInt xs


addTwos :: [Double] -> [Double]

addTwos [] = []
addTwos (x:xs)
    | null xs = []
    | len == 1 = [(x + head xs)]
    | otherwise = (x + head xs):addTwos (tail xs)
    where len = length xs