


{- Recursion: SPLIT WISELY, COMBINE CONVENIENTLY

   Any recursive function does the following:
        1. Check if no recursion is needed for the answer
            This is captured with a base case
        2.
            a) Prepare input for recursive calls
                Split the input into one or more simpler portions
                    SPLIT WISELY
            b) Call yourself recursively over all of the simpler/split inputs
                NOTE: Each successive call should get you closer to the base case
                    i.e. Every recursive call should use some proper subset of the current input
            c) Combine all the results from each recursive call to obtain a final answer
                i.e. Combine the puzzle pieces in a way that yields the desired operation
                    COMBINE CONVENIENTLY
                    
    Idea: We want to break apart some complex input into simpler inputs until we hit "rock bottom", at which point
        the answer is trivial (we hit a base case), apply the base case result and then backtrack to add on the solutions
        of each previous recursive call, each of which will ultimately add to obtain the final result.
        
        Break complex operands into their fundamental components, solve the now trivial problem and use the results to
            build up the complete solution for the original input.
            
        We SPLIT WISELY to get to the fundamental level (base case) at which point we reapply each new result
            with particular rules (COMBINE COVENIENTLY) as we backtrack through the recursion tree.
            
            
    NOTE: You may often find yourself needing to create/use helper functions to break up the necessary task
-}










-- Count the number of times an element appears in a list
-- NOTE: We are doing comparisons on the type represented by the type variable
--          therefore we need to ensure it has the equality type class constraint 
--          (effectively giving == and /= (Is Equal and Is Not Equal)
count :: (Eq a) => a -> [a] -> Int

-- Count the occurrences of v in the list
count v [] = 0 -- Base case, this is the trivial solution, the "rock bottom" in a recursion tree (at which point we move back up the tree)
count v (h:rest)
    | v == h = 1 + vrest -- We found an instance of the element in our list, add one to the result
    | otherwise = vrest -- Keep digging deeper, nothing found here so don't add to result.
    -- Here is the recursive call. Notice we send in an input that gets us closer to our base case (an empty list)
    where vrest = count v rest -- "rest" is the tail portion of our input list
-- Sample input = 2 [1,2,2,1,2] -> vrest -> 1 + vrest -> 1 + 1 + vrest -> 1 + 1 + vrest -> 1 + 1 + 1 + vrest -> 1 + 1 + 1 + 0 -> Result = 3
--                 Iteration #:      1          2             3                 4                5              Base Case
-- Conceptual view -> 1: No 2 at head. 2: Found an occurrence, add 1. 3: add 1. 4: nothing... 5: add 1. Base case pattern matched










-- Compress: Take a list and compress all blocks of consecutive element to a single element
comp :: (Eq a) => [a] -> [a]

-- Remove repeats in our list, effectively compressing our list down to remove redundant data
comp [] = [] -- Covers the case that we were provided an empty list
comp [x] = [x] -- Base case. This is needed to ensure that we have at least 2 elements for the below pattern
comp (x:y:rest) -- We grab the first 2 elements to check if they're equivalent (which would mean we should compress them)
    | x == y = comp (y:rest) -- They are the same element, compress by ignoring the first element (call self with everything else)
                             -- This also has the effect of checking if the head of 'rest' is yet another repeat by keeping the compressed element for the next call
    | otherwise = x:(comp(y:rest)) -- They are not equivalent, tack on the first element to a compressed version of everything else
                                   -- This is effectively saying "oh, 'x' is good, no need for further compression, take him out of the consideration"
-- Sample input = [1,2,2,1,2] -> 1:(comp (2:[2,1,2])) -> 1:(comp (2:[1,2]) -> 1:2:(comp (1:[2])) -> 1:2:1:(comp (2:[])) -> 1:2:1:[2] -> Result = [1,2,1,2]
--                  Iteration #:          1                      2                    3                    4               Base Case
-- Conceptual view -> 1: No match, take 1 out of consideration. 2: Match, compress 2. 3: No match. 4: No match. Base case pattern matched










-- Multiply two numbers using multiple addition and return the product
-- NOTE: Our technique chosen won't work with decimal numbers (our base case wouldn't be hit resulting in an infinite loop)
mult :: Integer -> Integer -> Integer

-- NOTE: A negative multiplicand (left operand, i.e. x) is fine since -2 * 3 = -2 + -2 + -2 = -6 and no adjustment to sign is needed
mult x 0 = 0 -- Base case, we have received 0 as a multiplier either as initial input or as result of recursion, either way result is 0
mult x y
    | y < 0 = (-mult x (-y)) -- If y is negative, we basically want a negative result of mult as if y was positive
    | otherwise = x + mult x (y-1) -- Add x to the result of mult with 1 less of a multiplier (right operand), since we just extracted an x for the addition
-- Sample input = 2 -3 -> -(mult 2 3) -> -(2 + (mult 2 2)) -> -(2 + 2 + (mult 2 1)) -> -(2 + 2 + 2 + (mult 2 0)) -> -(2 + 2 + 2 + 0) -> Result = -(2 + 2 + 2 + 0) = -6
--           Iteration #:      1                 2                      3                       4                       Base Case
-- Conceptual view -> 1: 3 was negative, negate the final answer. 2: Extract a 2 for the addition, reduce the multiplier. 3: Same thing. 4: Same thing. 5: Base case pattern matched










-- Reverse the items of a list
rev :: [a] -> [a]

rev [] = [] -- Base case, nothing to reverse
rev (x:xs) = -- x will be what we want on the end of the list of the result (hence reverse the list)
            (rev xs) ++ [x] -- We use ++ since colon (:) takes an element as LHS operand. We want the list to be what is to the left of our element (hence reverse the list)
-- Sample input = [1,2,3] -> (rev [2,3]) ++ [1] -> (rev [3]) ++ [2] ++ [1] -> (rev []) ++ [3] ++ [2] ++ [1] -> [] ++ [3] ++ [2] ++ [1] -> Result = [3,2,1]
--              Iteration #:           1                       2                           3                           Base Case                  
-- Conceptual view -> 1: Extract head of list for the RHS of result. 2: Keep expanding leftwards. 3: Same thing. 4. Base case pattern matched

-- Reverse the digits of a number
revno :: Int -> Int

-- revhelp does all the work. This helper function makes it so that a user can call revno conveniently without having to specify a every time it's called
revno n = revhelp 0 n


-- HELPER FUNCTION for revno
revhelp :: Int -> Int -> Int

-- acc keeps track of the current reversed number as it would be if we were to return it
-- We reverse by taking the tail end of the current number (n) and moving that to the tail of whatever is currently the reversed result
revhelp acc 0 = acc -- All the guts of our number has been moved over to acc, the reversed number is now represented by acc
revhelp acc n =
    let -- let is used to define some variables BEFORE they're used
        -- m is the part we would lose by moving the decimal point left 1, d is the part we need to still consider to reverse
        m = n `mod` 10 -- This is the digit we are "knocking off" the tail end of our number by moving the decimal to the left 1
                       -- This is the digit whose turn it is to be added onto the tail end of acc
        d = n `div` 10 -- This is the whole number part that would be left if we moved the decimal point to the left 1
                       -- This is the part of our number that still needs to be considered for reversing
    in -- in is the second part of the let statement, it is the place we use the defined variables
        revhelp (10 * acc + m) d -- 10*acc has the effect of moving over our current acc 1 decimal place
                                 -- We then tack on the portion that wouldn't be covered by d
                                 
-- Sample input = 0 5328
-- Call stack (in the order of the calls):
-- BOTTOM                                   revhelp (10 * 0 + 8)     532  -> m = 8    d = 532 -> Next acc = 8
--                                       -> revhelp (10 * 8 + 2)     53   -> m = 2    d = 53  -> Next acc = 82
--                                       -> revhelp (10 * 82 + 3)    5    -> m = 3    d = 5   -> Next acc = 823
--                                       -> revhelp (10 * 823 + 5)   0    -> m = 5    d = 0   -> Next acc = 8235
-- TOP                                   -> 8235                          -- ANSWER










-- SORTING ALGORITHMS BELOW








-- Sort a list using the strategy of placing an element into a sorted list
sort :: (Ord a) => [a] -> [a] -- Type class Ord (ordering) is needed because we will be using comparison operators on the type of the type variable

-- Essentially the goal will be to take a list, break it up into simpler portions until the result is trivial,
--   and then rebuild it placing a value at a time into the list using place to make the decision of where to place it
sort [] = [] -- Base case
sort (x:xs) = -- The head of the list will be what we wish to place into our sorted list
             place x (sort xs) -- Place 
-- Keep in mind that place won't be called until (sort xs) reduces down to a list since that is place's second operand
--      So (sort xs) will effectively serve to break our list down to a trivial empty list, at which time placing an
--          element into the list is trivial. We then combine and place an element into a list of 1 element, which place
--          handles quite well, over time we build up a list, all the while this list remains sorted because that is place'
--          job, to maintain the sortage of a sorted list.
--     The key is that we give place an empty list (sorted by default) to start with, place has the responsibility to
--          maintain this sorted list with each new element we give it. The recursion in sort is used to give place a trivial start.

-- Sample input = [2,3,1,2]
-- Call stack (in the order of the calls): 
-- BOTTOM           place 2 (sort [3,2,1])                                    
--               -> place 2 (place 3 (sort [2,1])) 
--               -> place 2 (place 3 (place 2 (sort[1]))) 
--               -> place 2 (place 3 (place 2 (place 1 (sort []))))
-- TOP           -> place 2 (place 3 (place 2 (place 1 [])))     - Place now has valid input and takes over
--                                                                 Placing 1 into an empty list is trivial

-- Popping off the stack from top (since we have a valid call that place can satisfy) -> Going from TOP to BOTTOM in above
--                  place 2 (place 3 (place 2 [1])               -- place 1 [] results in [1]
--               -> place 2 (place 3 [1,2])                      -- place 2 [1] results in [1,2]
--               -> place 2 [1,2,3]                              -- place 3 [1,2] results in [1,2,3]
-- ANSWER        -> [1,2,2,3]                                    -- place 2 [1,2,3] results in [1,2,2,3]   (See below sample input for place)      


-- HELPER FUNCTION for sort -> Place an element into a SORTED list in a way that maintains the sortage
-- ASSUMPTION: We were given a SORTED list in which to place our element.
place :: (Ord a) => a -> [a] -> [a] -- Type class Ord (ordering) is needed because we will be using comparison operators on the type of the type variable

place x [] = [x] -- Base case, placing an element into an empty list is trivial (adding the element won't mess with sortage)
place x (h:tail)
    | x <= h = x:h:tail -- The element is less than the head of a sorted list, thus it must belong at the front and no further digging is needed
    | otherwise = h:(place x tail) -- Head should definitely remain at front, but we need to dig deeper to decide where x should go
                                   -- We call place recursively which will sooner or later give us the valid location where x belongs in this sorted list
                                   -- This valid location may be at the very tail of the list, in which case the base case pattern is matched
                                   -- Or the valid location may be somewhere else in the list, in which case the first guard will catch
                                   -- No matter the base pattern to be matched, during the rewind (backtracking) of the calls, the new sorted list will be created (convenient combining)
-- Sample input: 8 [4,6,9]
-- Call stack (in order of the calls):
-- BOTTOM           4:(place 8 [6,9])
--               -> 4:6:(place 8 [9])
-- TOP           -> 4:6:8:9:[]                  -- "x <= h" guard is matched, serving as the base case

-- Sample input: 2 [1,2,3]
-- Call stack (in order of the calls):
-- BOTTOM           1:(place 2 [2,3])
-- TOP           -> 1:2:2:[3]                   -- The above call to place matches the pattern "x <= h", we have reached a base case












-- Sort a list by splitting a given list into 2 lists and then sorting the individual lists
-- NOTE: This is much like the "sort" function above.
--       We aim to break our input down to the point our helper function has a trivial start.
mergeSort :: (Ord a) => [a] -> [a] -- Type class Ord is here because we pass whatever types we have in this function to merge
                                   --   which needs functionality from Ord

mergeSort [] = []
mergeSort [x] = [x]
mergeSort list =
    let -- let is used to define some variables BEFORE they're used
        len = (length list) `div` 2 -- div is needed instead of '/' due to '/' requiring Fractional and not Int
        list1 = take len list -- Take the first portion of our list to form the first split list
        list2 = drop len list -- Drop the portion we just took in line above to form the second split list
    in -- in is the second part of the let statement, it is the place we use the defined variables
        merge (mergeSort list1) (mergeSort list2)
-- Same idea as for the sort function above: merge won't be called until we have broken down our original input to a valid pattern for merge
--     This valid pattern will happen to be one of the base cases of merge, which will allow us to start rebuilding the list for the final result

-- Sample input = [2,3,1,2]
-- Call stack (in the order of the calls):     -- NOTE: I am attempting to space things out to be clean and resemble a tree
-- BOTTOM           merge               (mergeSort [2,3])                     (mergeSort [1,2])
--                                              |                                    |
--                           ------------------------------------     ------------------------------------
--               -> merge   (merge (mergeSort [2]) (mergeSort[3]))   (merge (mergeSort [1]) (mergeSort[2]))
--                                        |               |                        |               |
--                                 -------------    -------------            ------------    -------------
-- TOP           -> merge   (merge (merge [] [2])  (merge [] [3]))   (merge (merge [] [1])  (merge [] [2]))
--                  At this point all calls have a pattern match in merge (no function being used)
-- NOTE: If only 1 or 2 branches had more of a list to break up, then those branches would of continued to span down until a pattern was matchable in merge
--       A call to merge like: "merge (mergeSort [2]) (mergeSort[3])" is unmatchable since merge doesn't take a function as input, it takes two lists

-- Popping off the stack from top (since we have a valid call that merge can satisfy) -> Going from TOP to BOTTOM in above
-- TOP              merge   (merge (merge [] [2])  (merge [] [3]))   (merge (merge [] [1])  (merge [] [2]))
--                                        |               |                        |               |
--                                       ---             ---                      ---             ---
--               -> merge   (merge       [2]             [3]     )   (merge       [1]             [2]     )
--                                               |                                         |
--                                             -----                                     -----
--               -> merge                      [2,3]                                     [1,2]
--                                                                 |
--                                                             ---------
-- BOTTOM                                                       [1,2,2,3]


-- HELPER FUNCTION for mergeSort -> Merge two SORTED lists
-- ASSUMPTION: We were given two SORTED lists
merge :: (Ord a) => [a] -> [a] -> [a] -- Type class Ord (ordering) is needed because we will be using comparison operators on the type of the type variable

-- The below two patterns are the base cases
merge xs [] = xs -- xs can be any list, even an empty list. So the case of both operands being empty lists is covered
merge [] ys = ys -- Like the above pattern, the merge of 2 lists where 1 is empty results in simply the nonempty list
merge (h1:t1) (h2:t2)
    | h1 <= h2 = h1:(merge t1 (h2:t2)) -- Since both lists are sorted, if h1 <= h2 we know h1 belongs at the front of results
                                        -- But can't say anything for sure about the rest, so merge what remains
    | otherwise = h2:(merge (h1:t1) t2) -- Same idea as above guard, but we know h2 belongs at the front of the results

-- Sample input: [1,4] [2,8]
-- Call stack (in order of the calls):
-- BOTTOM           1:(merge [4] (2:[8]))       -- "h1 <= h2" guard was matched
--               -> 1:2:(merge (4:[]) [8])      -- NOTE: 4:[] is syntactic sugar for [4], I am sticking to the format of the call
--               -> 1:2:4:(merge [] (8:[]))
-- TOP           -> 1:2:4:[8]                   -- merge of an empty list and a nonempty list is the nonempty list
                                                -- Thus we have reached the bottom of our recursive calls

-- Sample input: [2,3] [1,2]
-- Call stack (in order of the calls):
-- BOTTOM           1:(merge (2:[3]) [2])
--               -> 1:2:(merge [3] (2:[]))
--               -> 1:2:2:(merge (3:[]) [])
-- TOP           -> 1:2:2:[3]                   -- Base case pattern matched... That's all folks!











-- Sort a list by splitting up its tail into two lists
--                                                    One with all the elements less than or equal to the head of the list
--                                                    The other with all the elements greater than the head of the list
-- Essentially we will put the head of the list in the middle of these two new lists.. 
--      But these new lists will be treated the same... Up until we hit a base case. At which time combining the resulting list gives us (conveniently) the answer
quicksort :: (Ord a) => [a] -> [a] -- Type class Ord (ordering) is needed because we will be using comparison operators on the type of the type variable

quicksort [] = []
quicksort (h:tail) =
    let -- let is used to define some variables BEFORE they're used
        list1 = [a | a <- tail, a <= h] -- List comprehension that grabs all elements from the tail that are <= our head
        list2 = [a | a <- tail, a > h] -- List comprehension that grabs all elements from the tail that are > our head
    in -- in is the second part of the let statement, it is the place we use the defined variables
        (quicksort list1) ++ [h] ++ (quicksort list2) -- Sandwich the head inbetween the two other lists
                                                      -- Left side is conveniently all that is less than or equal to h
                                                      -- Right side is conveniently all that is greater than h
-- NOTE: You could do "(quicksort list1) ++ (h:(quicksort list2))" instead of "(quicksort list1) ++ [h] ++ (quicksort list2)"

-- Sample input = [2,3,1,2]
-- Call stack (in the order of the calls):     -- NOTE: I am attempting to space things out to be clean and resemble a tree
-- BOTTOM                    (quicksort [1,2])                                                     ++ [2] ++                                                     (quicksort [3])
--                                   |                                                                                                                                  |
--                   ---------------------------------------------------                                                                            ---------------------------------------
--               -> ((quicksort []) ++ [1] ++            (quicksort [2]))                          ++ [2] ++                                       ((quicksort []) ++ [3] ++ (quicksort []))
--                        |                                     |                                                                                        |                        |
--                       --                    --------------------------------------                                                                   --                        --
--                  (    []         ++ [1] ++ ((quicksort [] ++ [2] ++ quicksort [])))             ++ [2] ++                                       (    []         ++ [3] ++      []       )
--                                                  |                       |                                                                           
--                                                  --                      --                                                                  
--                  (    []         ++ [1] ++ (     []       ++ [2] ++      []       )             ++ [2] ++                                       (    []         ++ [3] ++      []       )

-- TOP                                                                                             [1,2,2,3]

-- NOTE: We could of added the pattern "quicksort [x] = [x]" below "quicksort [] = []" and it should still work the same, but the tree diagram would have been a bit simpler... :D






























