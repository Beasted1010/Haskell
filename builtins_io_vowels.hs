
-- NOTE: I use "error" this is just to be complete with how the built in responds.
-- If this is a problem, then please imagine the line removed


{- Known issues:
        putStr does not seem to work -> Terminal just waits for ever without printing prompt (seems to skip the line)
-}

-- This is the fix to the known issue above. Flushing the buffer...
import System.IO

-- Workaround for the issue with the buffer, source: https://stackoverflow.com/questions/13190314/io-happens-out-of-order-when-using-getline-and-putstr
prompt :: String -> IO String

prompt text = do
    putStr text
    hFlush stdout
    getLine

    
main = do
    text <- prompt "Text: "
    if null' text
    then 
        return ()
    else do
        let
            wordies = s2w text
            result = sum' [countVowels pali | pali <- wordies, isPalindrome pali]
        print result
        main





-------------------------------------------------------- Built-Ins --------------------------------------------------------

null' :: [a] -> Bool

null' [] = True
null' x = False


fst' :: (a,b) -> a

fst' (a, b) = a


snd' :: (a, b) -> b

snd' (a, b) = b


head' :: [a] -> a

head' [] = error "empty list"
head' (x:xs) = x


last' :: [a] -> a

last' [] = error "empty list"
last' (x:[]) = x
last' (x:xs) = last' xs


init' :: [a] -> [a]

init' [] = error "empty list"
init' (x:[]) = []
init' (x:xs) = x:(init' xs)


tail' :: [a] -> [a]

tail' [] = error "empty list"
tail' (x:[]) = []
tail' (x:xs) = [head' xs] ++ (tail' xs)


zip' :: [a] -> [b] -> [(a,b)]

zip' [] ys = []
zip' xs [] = []
zip' (x:xs) (y:ys) = (x,y):(zip' xs ys)


toUpper' :: Char -> Char

toUpper' c
    | null' charList = c
    | otherwise = snd' (head' charList)
    where charList = [x | x <- zip' ['a'..'z'] ['A'..'Z'], fst' x == c || snd' x == c]


sum' :: [Int] -> Int

sum' [] = 0
sum' (x:xs) = x + sum' xs


elem' :: (Eq a) => a -> [a] -> Bool

elem' x [] = False
elem' x (y:ys)
    | x == y = True
    | otherwise = elem' x ys






---------------------------------------------------------- WORDS ----------------------------------------------------------

-- Eat white spaces
eatSpaces :: String -> String

eatSpaces "" = ""
eatSpaces (c:s)
    | c == ' ' = eatSpaces s
    | otherwise = (c:s)
    
    
    
-- Extract a word
extractWord :: String -> String

extractWord "" = ""
extractWord (c:s)
    | c == ' ' = "" -- Want to check if next character is also a space or not
    | otherwise = c:(extractWord s)

    
    
-- Give the remaining string without any leading white spaces
eliminateWord :: String -> String

eliminateWord "" = ""
eliminateWord (c:s)
    | c == ' ' = eatSpaces s -- Return the remaining string without any white spaces
    | otherwise = eliminateWord s -- Lop off the character

    
    
s2w :: String -> [String]

s2w [] = []
s2w (c:s)
    | c == ' ' = s2w s
    | otherwise = (extractWord (c:s)):(s2w (eliminateWord (c:s)))


    
    
    
----------------------------------------------------- CUSTOM FUNCTIONS -----------------------------------------------------
    
isPalindrome :: String -> Bool

isPalindrome [] = True
isPalindrome (x:[]) = True
isPalindrome str
    | toUpper' (head' str) == toUpper' (last' str) = isPalindrome (init' (tail' str))
    | otherwise = False


isVowel :: Char -> Bool

isVowel c = c `elem'` ['a', 'e', 'i', 'o', 'u'] || c `elem'` ['A', 'E', 'I', 'O', 'U']


countVowels :: String -> Int

countVowels [] = 0
countVowels str = sum' [1 | x <- str, isVowel x]





















