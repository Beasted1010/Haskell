
-- Determine if input is a palindrome
isPalindrome :: (Eq a) => [a] -> Bool

isPalindrome [] = True
isPalindrome (x:xs)
    | null xs = True
    | x == last xs = isPalindrome (init xs)
    | otherwise = False


-- Compress a list by removing neighboring duplicates
compress :: (Eq a) => [a] -> [a]

compress [] = []
compress (x:xs)
    | null xs = [x]
    | x == head xs = [] ++ compress xs
    | otherwise = x:compress (xs)


-- Helper function for pack to grab the consecutive numbers at a head of a list
grabConsecutive :: (Eq a) => [a] -> [a]

grabConsecutive [] = []
grabConsecutive (x:xs)
    | null xs = [x]
    | x == head xs = x:grabConsecutive xs
    | otherwise = [x]


-- Create a list of lists that contain all the elements of the given list but containing any consecutive duplicates in a single sublist
pack :: (Eq a) => [a] -> [[a]]

pack [] = []
pack (x:xs) = 
    let
        sub_list = grabConsecutive (x:xs)
    in [sub_list] ++ pack (drop ((length sub_list) - 1) xs)


-- Take a packed list (a list of lists that contain duplicate neighbors together) and replace the list with a list of tuples
-- The tuples in the returned list indicate the number of repeats for the specified element
encodePacked :: (Eq a) => [[a]] -> [(Int, a)]

encodePacked [] = []
encodePacked (x:xs) = (length x, head x):encodePacked xs


-- Take a list and turn it into a list of tuples that skips the encoding step
--      goes straight to a list of tuples that specifies number of repeats for an element
encodeUnpacked :: (Eq a) => [a] -> [(Int, a)]

encodeUnpacked [] = []
encodeUnpacked (x:xs) =
    let
        sub_list = grabConsecutive (x:xs)
        len = length sub_list
    in (len, head sub_list):encodeUnpacked (drop (len - 1) xs)


-- Take a list of tuples that describe neighboring duplicates and create a list that matches the tuples description tuple = (numRepeats, elemRepeated)
decodeUnpacked :: (Eq a) => [(Int, a)] -> [a]

decodeUnpacked [] = []
decodeUnpacked (x:xs) =
    take len (repeat (snd x)) ++ decodeUnpacked xs
    where len = fst x


-- Take a list and duplicate each entry
dupli :: [a] -> [a]

dupli [] = []
dupli (x:xs) = x:x:dupli xs


-- Helper function for repli to repeat what is contained in one single list
createListOfRepeats :: a -> Int -> [a]

createListOfRepeats x 0 = []
createListOfRepeats x n = x:(createListOfRepeats x (n - 1))


-- Take a list and repeat each element a given number of times
repli :: [a] -> Int -> [a]

repli [] _ = []
repli xs 0 = []
repli (x:xs) n = (createListOfRepeats x n) ++ repli xs n


dropKthElem :: [a] -> Int -> [a]

dropKthElem [] _ = []
dropKthElem xs 0 = xs
dropKthElem (x:xs) 1 = xs -- Ignore the element to drop
dropKthElem (x:xs) n = x:dropKthElem xs (n-1) -- We want this element


dropFirstKElems :: [a] -> Int -> [a]

dropFirstKElems [] _ = []
dropFirstKElems xs 0 = xs
dropFirstKElems (x:xs) n = dropFirstKElems xs (n-1)


dropEveryNthHelper :: [a] -> Int -> Int -> [a] -- Int1 = Number to drop, Int2 = Current number

dropEveryNthHelper [] _ _ = []
dropEveryNthHelper xs 0 _ = []
dropEveryNthHelper (x:xs) n k
    | k == 1 = dropEveryNthHelper xs n n -- Drop the value
    | otherwise = x:(dropEveryNthHelper xs n (k-1)) -- Keep the value

-- RETHINK -> Works but there is a bug... ex: dropEveryNth [1,2,3] 5 -> "Non-exhaustive patterns"
dropEveryNth :: [a] -> Int -> [a]

dropEveryNth [] _ = []
dropEveryNth xs 0 = xs
dropEveryNth xs n = doDaDropDang xs n n
    where -- BUG IN THIS CODE ! GRR -> If string isn't of a length that is a multiple of n, then it's "non-exhaustive"
        doDaDropDang (x:xs) n k
            | k == n && null xs = [x]
            | k == 1 = doDaDropDang xs n n -- Drop the value
            | otherwise = x:(doDaDropDang xs n (k-1)) -- Keep the value


-- TODO: Do a recursive version
splitList :: [a] -> Int -> ([a], [a])

splitList [] _ = ([], [])
splitList xs 0 = ([], xs)
splitList xs n = (firstHalf, (drop (length firstHalf) xs))
    where 
        firstHalf = [fst y | y <- zip xs [0..], snd y < n]



sliceList :: [a] -> Int -> Int -> [a]

sliceList [] _ _ = []
sliceList xs 0 _ = []
sliceList xs _ 0 = []
sliceList (x:xs) n m 
    | n == 1 = [fst y | y <- zip (x:xs) [0..], snd y < slice] -- Grab our slice. x:xs is our remaining list
    | otherwise = sliceList xs (n-1) (m-1) -- Adjust our index bounds to compensate for shorter list
    where slice = m - n + 1 -- Bounds are inclusive


-- Rotate the list to the right. Rotate to the left if integer input is negative (integer input is number of spaces to rotate list)
rotateListRight :: [a] -> Int -> [a] -- Default is to move list to right, negative numbers move list to left

rotateListRight [] _ = []
rotateListRight xs 0 = xs
rotateListRight (x:xs) n
    | n < 0 = rotateListRight (xs ++ [x]) (n+1) -- Move list to the left, tacking on the dropped element to other side
    | otherwise = rotateListRight ([last xs] ++ (x:(init xs))) (n-1) -- Move list right, tack dropped element on left


-- Remove an element from a given array at a given location
removeAt :: [a] -> Int -> [a]

removeAt [] _ = []
removeAt (x:xs) n
    | n == 1 = xs
    | n == 0 = (x:xs)
removeAt (x:xs) n = x:removeAt xs (n-1)


-- Insert an element at a given spot in a given array
insertAt :: (Show a) => a -> [a] -> Int -> [a]

insertAt x [] n = [x]
insertAt x ys 1 = (x:ys)
insertAt x (y:ys) n
    | n < 1 = error ("Can't insert " ++ show x ++ " into the list at a location less than 1. You entered: " ++ show n)
    | otherwise = y:(insertAt x ys (n-1))


-- Print out numbers from start to finish (inclusive)
-- Cool function that uses a helper function in the where clause
range :: Int -> Int -> [Int]

range l r = 
    let
        diff = r - l + 1
    in
        iterate diff l
    where
        iterate 0 lf = []
        iterate d lf = lf:(iterate (d-1) (lf + 1))


-- Helper function for lensort that will place an element (a list) into a list of lists to maintain sortage (sort by length)
place :: [a] -> [[a]] -> [[a]]

place [] xs = [] ++ xs
place x [] = [x] -- Base case
place x (h:t)
    | length x <= length h = x:(place h t)
    | otherwise = h:(place x t)


-- Sort list of lists based on length of the sublists
lensort :: [[a]] -> [[a]]

lensort [[]] = [[]]
lensort [x] = [x] -- Base case (the rebound point)
lensort (h:t) = place h (lensort t)


isPrime :: Int -> Bool

isPrime n = null [x | x <- [2..n-1], n `mod` x == 0] -- List of factors of n that don't include 1 and itself, if null then prime.


-- Function to return the Fibonacci number at user specified location
fibonacci :: Int -> Int

fibonacci 1 = 1
fibonacci 2 = 1
fibonacci x = fibonacci (x-1) + fibonacci(x-2)


-- Function to print the first user specified Fibonacci numbers
printFibonacci :: Int -> [Int]

printFibonacci x = [fibonacci x | x <- [1..x]]






































