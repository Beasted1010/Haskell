
main :: IO ()
main = return ()



{- A longer version with use of a helper function
collatz_helper :: Int -> Int -> Int

collatz_helper 1 n = n
collatz_helper x n
    | x `mod` 2 == 0 = collatz_helper (x `div` 2) (n + 1)
    | otherwise = collatz_helper ((3 * x) + 1) (n + 1)


collatz :: Int -> Int

collatz x
    | x <= 0 = (-1)
    | otherwise = collatz_helper x 0
-}
    
-- A more elegant and recursion friendly way
collatz 1 = 0
collatz x
    | x <= 0 = (-1)
    | x `mod` 2 == 0 = (collatz (x `div` 2)) + 1
    | otherwise = (collatz ((3*x) + 1)) + 1


place :: [a] -> [[a]] -> [[a]]

place [] xs = [] ++ xs
place x [] = [x] -- Base case
place x (h:t)
    | length x <= length h = x:(place h t)
    | otherwise = h:(place x t)


-- Sort list of lists based on length of the sublists
lensort :: [[a]] -> [[a]]

lensort [[]] = [[]]
lensort [x] = [x] -- Base case (the rebound point)
lensort (h:t) = place h (lensort t)



merge :: [[a]] -> [[a]] -> [[a]]

merge [] ys = ys
merge xs [] = xs
merge (h1:t1) (h2:t2)
    | length h1 <= length h2 = h1: merge t1 (h2:t2)
    | otherwise = h2: merge (h1:t1) t2

mergesort :: [[a]] -> [[a]]

mergesort [] = []
mergesort [x] = [x]
mergesort xs =
    let
        list1 = take (length xs `div` 2) xs
        list2 = drop (length xs `div` 2) xs
    in
        merge (mergesort list1) (mergesort list2)





















