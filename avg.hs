-- A program to read multiple numbers, one per line, until terminated by 0,
-- and output the average of all non-zero numbers read

-- main

-- The mainline simply gets a list of all values entered, and prints the
-- average of all values in the list.  The actual input of values, and
-- list construction is done by readNums.

main :: IO ()

main = do
         putStrLn "Enter values, one per line, terminated by 0"
         lst <- readNums
         putStr "The average of your non-zero numbers is "
         print ((sum lst) / fromIntegral (length lst))

-- readNums

-- Reads multiple numbers, one per line, until terminated by 0.
-- Returns an I/O action whose value is the list of all non-zero numbers read.

readNums :: IO [Double]

readNums = do
             line <- getLine
             let num = read line :: Double
             if num == 0
               then return []
               else do
                      lst <- readNums
                      return (num:lst) 
