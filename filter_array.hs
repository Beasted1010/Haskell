
-- Filter an array of integers for all that are less than a given integer n
filter_array :: Int -> [Int] -> [Int]

filter_array n [] = [] 
filter_array n (h:t)
    | h >= n = filter_array n t -- Filter out this item, we only want those LESS THAN n 
    | otherwise = h:(filter_array n t)


find_GCD :: Int -> Int -> Int

find_GCD x y
    | x == y = x
    | x > y = find_GCD (x-y) y
    | otherwise = find_GCD x (y-x)

-- Find the smallest multiple of these numbers that they both share
find_LCM :: (Enum a, Eq a, Num a, Integral a) => a -> a -> a

find_LCM x y =
    let
        x_factors = [f | f <- [1..x], x `mod` f == 0]
        y_factors = [f | f <- [1..y], y `mod` f == 0]
        my_max = max x y
        
        -- NOTE: Broken, definitely not right. SO BROKEN. Obviously broken. ... GRRR
        com_factors = [com_f | com_f <- [1..my_max], (com_f `elem` x_factors) || (com_f `elem` y_factors)]
        
    in
        mult_list com_factors
    
    
mult_list :: (Num a) => [a] -> a

mult_list [x] = x
mult_list (h:t) = h * mult_list t

mingle_string :: String -> String -> String

mingle_string str1 [] = []
mingle_string [] str2 = []
mingle_string (h1:t1) (h2:t2) = h1:h2:(mingle_string t1 t2)

