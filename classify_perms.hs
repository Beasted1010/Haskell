-- Here to satisfy GHC requirements (if were to not load as module, but create executable)
main :: IO ()
main = return ()

classify :: Int -> String

classify n
    | n <= 0 = "Illegal"

classify n
    | result < n = "Deficient"
    | result == n = "Perfect"
    | result > n = "Abundant"
    where result = sum [x | x <- [1..n-1], n `rem` x == 0]
    
perms :: (Eq a) => [a] -> [a] -> Bool

perms xs ys
    | length xs /= length ys = False
    -- Pull every item of xs that is in ys -> Should be same length
    | length [x | x <- xs, x `elem` ys] /= length ys = False
    -- Pull every item of ys that is in xs -> Should be same length
    | length [y | y <- ys, y `elem` xs] /= length xs = False
    -- If we made it this far then we are good
    | otherwise = True