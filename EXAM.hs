

-- putStr "Put me on the line"
-- putStrLn "Put me on the line\n"
-- putChar 'a'
-- ch <- getChar
-- ch <- getLine

-- do        chains IO commands together (everything returned after do must be IO action)
-- return    encapsulates value into box -> return () = NO-OP


-- print     displays a value of any type
-- show      converts a printable value to a string (opposite of read)
-- read      takes STRING and gives contents as some specified type
--                  MUST BE GIVEN (if not implicit) -> let val = read nums :: [Integer]




main :: IO () -- NOTE: SOME TYPE MAY REPLACE ()

main = do
    putStrLn "Enter in an expression to be calculated."
    expr <- getLine
    if null expr
        then
            return ()
        else do
            let lhs = read (parseFirst expr) :: Integer
            let rest = removeFirst expr
            let op = head rest
            let rhs = read (tail rest) :: Integer
            let result = calc_answer lhs rhs op
            print result
            main
        
    
    
calc_answer lhs rhs op =
    if op == '+'
        then lhs + rhs
        else lhs - rhs
    
    

calc :: String -> Integer

calc str =
    let
        lhs = read (parseFirst str) :: Integer
        rest = removeFirst str
        op = head rest
        rhs = read (tail rest) :: Integer
    in
        if op == '+'
            then lhs + rhs
            else lhs - rhs


parseFirst :: String -> String

parseFirst (h:t)
    | h == '+' || h == '-' = []
    | otherwise = h:(parseFirst t)
    

removeFirst :: String -> String

removeFirst (h:t)
    | h == '+' || h == '-' = (h:t)
    | otherwise = (removeFirst t)
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
  
  
  
mystery x = [sum (take i x) | i <- [1..length x]]
mys :: [Integer] -> [Integer]
mys [] = []
mys [x] = [x]
mys x = (mys (init x)) ++ [sum x]